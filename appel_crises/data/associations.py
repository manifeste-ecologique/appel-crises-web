ASSOCIATIONS = [
    ("http://www.tourisme-durable.org/", "Acteurs du Tourisme Durable"),
    ("https://www.adrmarine.org/", "Association de Défense des Ressources Marines"),
    ("http://www.alter-actions.org/", "Alter'Actions"),
    ("https://www.animafac.net/", "Animafac"),
    ("https://www.apf-francehandicap.org/", "APF France handicap"),
    (
        "https://www.anemf.org/",
        "Association Nationale des Etudiants en Médecine de France",
    ),
    ("https://negawatt.org/", "Association négaWatt"),
    (
        "https://www.aspas-nature.org/",
        "Association pour la Protection des Animaux Sauvages",
    ),
    ("https://www.atd-quartmonde.fr/", "ATD-Quart Monde"),
    ("https://avenirclimatique.org/", "Avenir Climatique"),
    ("https://www.facebook.com/fededesassosaix/", "B.A.-BA FAEAP"),
    ("https://www.cacommenceparmoi.org", "Ça Commence par Moi"),
    ("https://campus-transition.org/", "Campus de la Transition"),
    ("https://lobby-citoyen.org/", "Citoyennes.ens Lobbyistes d'Intérêts Communs"),
    ("https://citoyenspourleclimat.org/", "Citoyens Pour Le Climat"),
    ("https://www.weareclimates.org/", "CliMates"),
    ("https://climatoptimistes.org/fr/", "Climat'Optimistes"),
    ("https://www.colibris-lemouvement.org/", "Colibris"),
    ("http://www.cddd.fr/", "Collège des Directeurs de Développement Durable"),
    ("http://www.comite21.org/", "Comité 21"),
    (
        "https://junior-entreprises.com/",
        "Confédération Nationale des Junior-Entreprises",
    ),
    ("https://convergenceinfirmiere.com/", "Convergence Infirmière"),
    ("https://cop2etudiante.org/", "COP2 étudiante"),
    ("https://earthuprising.org/", "Earth Uprising France"),
    ("https://emmaus-france.org", "Emmaüs"),
    ("https://enseignantspourlaplanete.com/", "Enseignant.e.s Pour La Planète"),
    ("http://www.entrepreneursdavenir.com/", "Entrepreneurs d'Avenir"),
    ("https://extinctionrebellion.fr/", "Extinction Rebellion - Groupe ACR"),
    ("https://www.fage.org/", "FAGE"),
    ("http://fnesi.org/", "Fédération Nationale des Etudiant.e.s en Soins Infirmiers"),
    ("https://fidl.org/", "FIDL"),
    ("https://filiere-paysanne.blogspot.com/", "Filière Paysanne"),
    ("http://www.fondation-nature-homme.org/", "Fondation Nicolas Hulot"),
    ("https://www.football-ecology.org/", "Football et Ecologie"),
    ("https://forumfrancaisjeunesse.fr/", "Forum Français de la Jeunesse"),
    ("https://www.fne.asso.fr/", "France Nature Environnement"),
    ("https://giletscitoyens.org/", "Gilets Citoyens"),
    ("https://www.facebook.com/greenlobby.citoyen/", "Greenlobby"),
    (
        "https://www.groupe-sos.org/transition-ecologique",
        "Groupe SOS Transition écologique",
    ),
    ("https://www.i-boycott.org/", "I-boycott"),
    ("https://ilestencoretemps.fr/", "Il est Encore Temps"),
    ("https://www.institut-rousseau.fr/", "Institut Rousseau"),
    ("https://www.veblen-institute.org/", "Institut Veblen"),
    ("http://www.jac-asso.fr/", "Jeunes Ambassadeurs pour le Climat"),
    ("https://fresqueduclimat.org/", "La Fresque du Climat"),
    ("https://www.laseinenestpasavendre.com/", "La Seine n'est pas à vendre"),
    ("https://labos1point5.org/", "Labos 1.5"),
    ("https://www.latitudes.cc/", "Latitudes"),
    ("http://lemonsea.org/", "Leamonsea"),
    ("https://resiliencealimentaire.org/", "Les Greniers d'Abondance"),
    ("https://theshiftproject.org/equipe/", "Les Shifters"),
    ("https://www.littlecitizensforclimate.org/", "Little Citizen for Climate"),
    ("https://lowcarbonfrance.org/", "Low Carbon France"),
    ("https://makesense.org/", "Makesense"),
    ("http://miramap.org/", "Miramap"),
    ("https://reseaumycelium.org/index.php/lassociation/", "Mycelium"),
    ("https://www.the-noise.org/", "NOISE"),
    ("https://notreaffaireatous.org/", "Notre Affaire A Tous"),
    ("https://www.onestpret.com/", "On Est Prêt"),
    ("https://www.facebook.com/parisbascule/", "Paris-Bascule"),
    ("https://pour-un-reveil-ecologique.org/fr/", "Pour un réveil écologique"),
    ("https://printemps-ecologique.fr/", "Printemps Ecologique"),
    ("https://profsentransition.com/", "Profs en Transition"),
    ("https://refedd.org/", "REFEDD"),
    ("https://reseauactionclimat.org/", "Réseau Action Climat"),
    ("http://www.reseau-environnement-sante.fr/", "Réseau Environnement Santé"),
    ("https://resilience-france.org/", "Resilience"),
    ("https://sciencescitoyennes.org/", "Sciences Citoyennes"),
    ("https://sosmaires.org/", "SOS Maires"),
    ("https://www.together-for-earth.org/", "Together For Earth"),
    ("https://theshiftproject.org/", "The Shift Project"),
    ("http://unef.fr/", "UNEF"),
    ("https://www.facebook.com/UnisPourLeClimat/", "Unis Pour Le Climat"),
    ("https://www.unicef.fr/", "UNICEF France"),
    ("https://www.facebook.com/groups/unis.pour.la.planete/", "Unis Pour La Planète"),
    ("https://www.uriopss-nouvelleaquitaine.fr/", "URIOPSS Nouvelle-Aquitaine"),
    ("http://wearereadynow.net/", "WARN"),
    ("https://www.wwf.fr/", "WWF"),
    ("https://www.facebook.com/associationYONALOT/", "Yon A Lot - L'Un à l'Autre"),
    ("https://youthforclimate.fr/", "Youth For Climate France"),
]
